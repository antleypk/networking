import csv, sys, random, datetime
import config


output = config.attendees
names = [
    "tim",
    "heather",
    "ranger",
    "charlie",
    "tom",
    "peter",
    "akiva",
    "aamir",
    "parker",
    "liam",
    "jj",
]


def main(people_count: int):
    # generate csv of people.

    print("     main")
    lcl_names = []
    # print(f"hello world, people count: {people_count} ")
    print("     generate list")
    for i in range(0, people_count):
        # print(f"i: {i}")
        index = random.randint(0, len(names) - 1)
        name = names[index]
        lcl_int = str(random.randint(10000, 99999))

        lcl_names.append(name + lcl_int)

    print("     save list")
    with open(output, "w", newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=",")
        count = 1
        for n in lcl_names:
            writer.writerow([count, n, datetime.datetime.now()])
            count += 1
    print("complete\n")


if __name__ == "__main__":
    print("\nstart")
    try:
        # arg 1: number of people
        people_count = int(sys.argv[1])
        main(people_count)
    except IndexError:
        print("     Error")
        print("     STD:IN $1, person count required (int)")
