import csv, sys, random, datetime, time, os
import config
import utility as util
import analyser


def get_data(attendees):
    return util.get_attendee_data(attendees)


def shuffle(pv_list):
    rv_list = []
    length = len(pv_list) - 1
    tmp_list = []
    for i in pv_list:
        tmp_list.append(i)
    while length > 0:
        index = random.randint(0, length)
        rv_list.append(tmp_list[index])
        tmp_list.pop(index)

        length -= 1

    return rv_list


def fill_tables(table_size, table_count, people):
    people_size = len(people)
    # print(f"\n fill tables: {table_count}, people size: {people_size} \n")
    shuffled = shuffle(people)
    tables = []
    person_index = 0

    for i in range(0, table_count):
        # print(f"table: {i}")
        person_count = 0
        lcl_table = {"id": i}
        seating_chart = []
        for index in range(person_index, people_size):
            # print(f"person: {index}, person_count: {person_count}, table size: {table_size}, person_index: {person_index}, people_size: {people_size}")
            if person_count < table_size:
                seating_chart.append(shuffled[index - 1])
                person_index += 1
                person_count += 1
        print(" ")
        lcl_table["seating_chart"] = seating_chart
        tables.append(lcl_table)
    left_over = []
    if person_index < people_size:
        for i in range(person_index, people_size):
            left_over.append(shuffled[i - 1])
    return tables, left_over


def run_event(table_size, table_count, people, number_of_turns):
    iterations = []
    for i in range(0, number_of_turns):
        people_count = len(people)
        seating_chart, left_over = fill_tables(table_size, table_count, people)
        iterations.append({"id": i, "table": seating_chart, "left_over": left_over})
    return iterations


def iteration_printer(iteration):
    util.iteration_printer(iteration)


def save_human(event):
    util.save_human(event)


def save_table(event):
    util.save_table(event)


def save_master(event):
    return util.save_master(event)


def save(event):
    save_human(event)
    save_table(event)
    return save_master(event)


def event_printer(event):
    util.event_printer(event)


def main(table_size, number_of_turns, table_count):
    attendees = config.attendees
    people = get_data(attendees)
    print("people: {}".format(people))
    event_seating = run_event(table_size, table_count, people, number_of_turns)
    iteration_printer(event_seating)
    path = save(event_seating)
    print(path)
    analyser.main(path)


if __name__ == "__main__":
    table_size = int(sys.argv[1])
    number_of_turns = int(sys.argv[2])
    table_count = int(sys.argv[3])
    main(table_size, number_of_turns, table_count)
