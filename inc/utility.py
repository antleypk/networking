import config, csv, time, datetime, os


def get_attendee_data(attendees_path: str):
    csv = get_data(attendees_path)
    data = []
    count = 0
    for i in csv:
        count + 1
        lcl_i = i.split(",")
        data.append((lcl_i[0], lcl_i[1]))
    return data


def get_data(path: str):
    return open(path)


def event_printer(event: dict):
    for iteration in event:
        iteration_printer(iteration)


def save_master(event: dict):
    if not os.path.isdir("./data"):
        os.mkdir("data")
    time_string = str(time.time()).split(".")[0]
    file_path = "{}_{}.csv".format(config.master_record, time_string)
    with open(file_path, "w") as chart:
        chart.write("turn,table,id,name\n")
        for i in event:
            # print("i keys: {}".format(i.keys()))
            for table in i["table"]:
                for person in table["seating_chart"]:
                    c = "{},{},{},{}\n".format(
                        i["id"], table["id"], person[0], person[1]
                    )
                    chart.write(c)
    return file_path


def save_table(event: dict):
    print("     save table\n")

    ttables = []

    for turn in event:
        # print(turn.keys())
        for tables in turn["table"]:
            # input(tables.keys())
            lcl_id = tables["id"]
            lcl_table = []

            for tu in event:
                tmp_table = []
                for table in tu["table"]:
                    if table["id"] == lcl_id:
                        for person in table["seating_chart"]:
                            # print("person: {}".format(person))
                            tmp_table.append(person)

                lcl_table.append(
                    {"table": lcl_id, "turn": tu["id"], "roster": tmp_table}
                )
            ttables.append((lcl_id, lcl_table))

    with open(config.table_chart, "w") as chart:
        chart.writelines("Table Seating Chart\n")
        chart.writelines("      Magyk Software")
        for a in ttables:
            d = "table: {}\n".format(a[1][1]["table"])
            chart.write(d)
            for b in a[1]:
                e = "     turn: {}\n".format(b["turn"])
                chart.write(e)
                for c in b["roster"]:
                    f = "         {}: {}\n".format(c[0], c[1])
                    chart.write(f)
            g = "\n"
            chart.write(g)


def save_human(event: dict):
    with open(config.master_seating_chart, "w") as chart:
        chart.write("Seating Chart\n    By Magyk Software\n\n")
        for i in event:
            a = "     Turn: {}\n".format(i["id"])
            chart.write(a)
            for table in i["table"]:
                b = "\n        Table: {}\n".format(table["id"])
                chart.write(b)
                for person in table["seating_chart"]:
                    c = "        {}: {}\n".format(person[0], person[1])
                    chart.write(c)
                if len(table["seating_chart"]) == 0:
                    d = "         Empty Table\n"
                    chart.write(d)
            if len(i["left_over"]) > 0:
                e = "\n         Unseated People\n"
                chart.write(e)
                for l in i["left_over"]:
                    f = "         {}: {}\n".format(l[0], l[1])
                    chart.write(f)
            chart.write("\n")


def iteration_printer(iteration: dict):
    for i in iteration:
        print("     Iteration: {}".format(i["id"]))
        for table in i["table"]:
            print(
                "\n        Table: {}\n        Leader: {}".format(
                    table["id"], table["leader"][1]
                )
            )
            for person in table["seating_chart"]:
                # input(person)
                if len(person) == 2:
                    print("        {}: {}".format(person[0], person[1]))
                if len(person) == 4:
                    print(
                        "        {}: {} Local Known: {}, METCOUNT: {}".format(
                            person[0], person[1], person[2], len(person[3])
                        )
                    )
            if len(table["seating_chart"]) == 0:
                print("         Empty Table")
        if len(i["left_over"]) > 0:
            print("\n         Unseated People")
            for l in i["left_over"]:
                print("         {}: {}".format(l[0], l[1]))
            print(" ")
        print(" ")
