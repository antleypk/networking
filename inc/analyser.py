import csv, sys, random, datetime, time, os
import config
import utility as util


def get_data(path):
    data = []
    count = 0
    for d in util.get_data(path):
        if count > 0:
            data.append(d.rstrip())
        count += 1

    return data


def get_people(data):
    people = set()
    for dd in data:
        d = dd.split(",")
        people.add(d[2])
    return people


def get_tables(data):
    tables = set()
    for tt in data:
        t = tt.split(",")
        tables.add(t[1])
    return tables


def get_turns(data):
    turns = set()
    for tt in data:
        t = tt.split(",")
        turns.add(t[0])
    # input(f"get turns{turns}")
    return turns


def get_user_tables(person_id, turns, data):
    tables = []
    # print("turns: {}".format(turns))
    for t in turns:
        for dd in data:
            d = dd.split(",")
            # print("d: {}".format(d))
            if d[0] == t:
                # print(d[0])
                if d[2] == person_id:
                    tables.append({"turn": t, "table": d[1]})
    return {"person_id": person_id, "table_ids": tables}


def analyse(data):
    tables = get_tables(data)
    turns = get_turns(data)
    people = get_people(data)

    people_size = len(people)
    print(f"people size: {people_size}")

    distribution = []

    for person in people:
        lcl_tble = get_user_tables(person, turns, data)
        # print("tables: {}".format(lcl_tble))
        person_id = lcl_tble["person_id"]
        lcl_tables = lcl_tble["table_ids"]
        connections = set()
        for i in lcl_tables:
            lcl_table = i["table"]
            lcl_turn = i["turn"]
            for i in get_connections(lcl_table, lcl_turn, data, person_id):
                connections.add(i)
        distribution.append(
            {"id": person_id, "connections": connections, "count": len(connections)}
        )
        # input(connections)

    return distribution


def get_connections(table, turn, data, person_id):
    # print(f"table: {table},turn {turn}")
    connections = set()
    for dd in data:
        d = dd.split(",")
        if d[0] == turn:
            if d[1] == table:
                if d[2] != person_id:
                    connections.add(d[2])

    return connections


def main(path):
    print(f"path: {path}")
    data = get_data(path)
    distribution = analyse(data)
    count = 0
    for d in distribution:
        count += d["count"]
        # lcl_d = [d["id"], d["count"]]
        # print(lcl_d)

    size = len(distribution)
    avg = count / size
    print(f"size: {size}, count: {count}, avg: {avg}")


if __name__ == "__main__":
    path = sys.argv[1]
    main(path)
