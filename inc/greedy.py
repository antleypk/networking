import utility as util
import config
import json, sys
import analyser


def get_data(attendees):
    return util.get_attendee_data(attendees)


def generate_history_schema(people: list, leaders: list):
    # add leaders to schema
    for leader in leaders:
        people.append(leader)

    # make a row in a json for each person
    schema = {}
    for person in people:
        lcl_id = person[0]
        lcl_name = person[1]
        met = []
        schema[lcl_id] = {"name": lcl_name, "met": met, "id": lcl_id}
    return schema


def create_tables(leaders: list):
    tables = []
    for i in range(0, len(leaders)):
        leader = leaders[i]
        leader_id = leader[0]
        tables.append({"id": i, "seating_chart": [leader], "leader": leader})
    return tables


def fill_tables(people: dict, leaders: dict, schema: dict, table_size: int):
    # print("fill tables")
    person_index = 0
    people_size = len(people)
    tables = create_tables(leaders)
    left_over = []

    for person in people:
        person_schema = schema[person[0]]
        person_id = person[0]
        # print(lcl_schema)
        lcl_tables = []

        for table in tables:
            table_id = table["id"]
            met = person_schema["met"]
            tbl_count = len(tables)

            leader = table["leader"]
            # met.append(leader[0])

            count = 0
            full = False
            if len(table["seating_chart"]) == table_size:
                full = True

            count = 0
            chart = table["seating_chart"]
            for c in chart:
                #  input(f"c in chart: {c}")
                lcl_id = c[0]
                for m in met:
                    #     input(f"m: {m}")
                    if lcl_id == m:
                        count += 1

            frame = {"table": table_id, "count": count, "full": full, "leader": leader}
            lcl_tables.append(frame)

        low = sorted(lcl_tables, key=lambda x: x["count"])

        added = False

        for l in low:
            if l["full"] == False:
                if added == False:
                    # input(person)
                    lcl_id = person[0]
                    lcl_name = person[1]
                    s_person = schema[lcl_id]
                    lcl_met = s_person["met"]
                    lcl_person = (lcl_id, lcl_name, l["count"], lcl_met)
                    lcl_table = l["table"]
                    tables[lcl_table]["seating_chart"].append(lcl_person)
                    print(f"add: {lcl_person}, table: {lcl_table}")
                    added = True

        if added == False:
            if int(person[0]) < 1000:
                left_over.append(person)

    return tables, left_over


def update_schema(schema: dict, seating_chart: dict):
    for table in seating_chart:
        meet_ids = []
        for chart in table["seating_chart"]:
            meet_id = chart[0]
            meet_ids.append(meet_id)
        for m in meet_ids:
            for n in meet_ids:
                if m != n:
                    print("m: {}, n:{}".format(m, n))
                    schema[n]["met"].append(m)

    return schema


def print_schema(schema: dict):
    for i in schema:
        # print(schema[i])
        print(
            "     ID: {} name: {}, met: {}".format(
                i, schema[i]["name"], schema[i]["met"]
            )
        )


def run_event(
    people: dict, schema: dict, table_size: int, leaders: dict, number_of_turns: int
):
    master_chart = []

    for i in range(0, number_of_turns):
        seating_chart, left_over = fill_tables(
            people, leaders, schema, table_size
        )  # fill the tables, make seating chart for one turn
        master_chart.append({"id": i, "table": seating_chart, "left_over": left_over})
        schema = update_schema(schema, seating_chart)
    return master_chart


def iteration_printer(iteration: dict):
    util.iteration_printer(iteration)


def save(event: dict):
    return util.save_master(event)


def build_leaders(count: int):
    leaders = []

    for a in range(0, leader_count):
        lcl_id = int(f"1000{a}")
        leaders.append((lcl_id, f"leader_{a}"))

    return leaders


def main(table_size: int, number_of_turns: int, leader_count: int):
    leaders = build_leaders(leader_count)

    people = get_data(config.attendees)  # get list of people

    schema = generate_history_schema(
        people, leaders
    )  # build chart to keep track of who meets who
    event = run_event(
        people, schema, table_size, leaders, number_of_turns
    )  # loop through number of terms and match people
    path = save(event)

    util.save_human(event)
    iteration_printer(event)

    analyser.main(path)


if __name__ == "__main__":
    table_size = int(sys.argv[1])
    number_of_turns = int(sys.argv[2])
    leader_count = int(sys.argv[3])
    main(table_size, number_of_turns, leader_count)
